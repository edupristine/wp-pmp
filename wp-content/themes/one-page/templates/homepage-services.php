<!--service section-->
<section id="services" class="section_2">
    <div class="services_div">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="main_head animated fade_in_up text-center"><?php echo esc_attr(onepage_get_option('onepage_service_section_heading', __('Four Column Feature Area ','one-page'))); ?></h2>
                    <hr class="service_sep animated fade_in_up" style="display:none;">
                    <p class="main_desc animated fade_in_up">PMP® certification is globally recognized and demanded, the PMP credential demonstrates to employers, clients and colleagues that a project manager possesses project management knowledge, experience and skills to bring projects to successful completion.</p>
<p class="main_desc animated fade_in_up">The Project Management Framework embodies a project life cycle and five major project management Process Groups.</p>
<ol start="1" class="main_desc animated fade_in_up">
<li>Initiating</li>
<li>Planning</li>
<li>Executing</li>
<li>Monitoring and Controlling</li>
<li>Closing</li>
<li>Encompassing a total of 47 processes.</li>
</ol>
<p class="main_desc animated fade_in_up">The processes of these knowledge areas are described by their inputs, tools and techniques, and outputs. The PMBOK also emphasizes the interaction and interdependence between different process groups. For example, the outputs from one process may be used by one or more other processes as inputs
</p>
                </div>
            </div>
            <div class="row">
                <!-- Service Box 1 -->
                <div class="col-md-6">
                    <div class="services_item animated fade_in_up" style="animation-delay: .3s">
                        <span class="glyphicon <?php echo esc_attr(onepage_get_option('onepage_service_box_icon_1', 'glyphicon-paperclip')); ?> one"></span>
                        <h4><?php echo esc_attr(onepage_get_option('onepage_service_box_heading_1', __('Completely Responsive','one-page'))); ?></h4>
                        <p><?php echo esc_attr(onepage_get_option('onepage_service_box_desc_1', __('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry','one-page'))); ?></p>
                    </div>
                </div>
                <!-- /Service Box 1 -->
                <!-- Service Box 2 -->
                <div class="col-md-6">
                    <div class="services_item animated fade_in_up" style="animation-delay: .6s">
                        <span class="glyphicon <?php echo esc_attr(onepage_get_option('onepage_service_box_icon_2', 'glyphicon-paperclip')); ?> one"></span>
                        <h4><?php echo esc_attr(onepage_get_option('onepage_service_box_heading_2', __('Multiple Colors','one-page'))); ?></h4>
                        <p><?php echo esc_attr(onepage_get_option('onepage_service_box_desc_2', __('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry','one-page'))); ?></p>
                    </div>
                </div>
                <!-- /Service Box 2 -->
                
            </div>
        </div>
    </div>
</section>
<!--/service section-->

