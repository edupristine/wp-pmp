<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_pmp_new');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '@11in0n3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<9=ijNxd,x!%#-YU`3`|~1kU@Qpd/SmO#jP59/jKir}s[CLv{p!:X.. jGJ o}@*');
define('SECURE_AUTH_KEY',  ';z~YG3*.w9`Zl|PH@B)WX?8N.L?c@i4u+bQHJMT,[EMVa[+(bn%7yZYDeGfcXL2,');
define('LOGGED_IN_KEY',    ':B.|hT(GUAgRP*.|6(Ywdf6Vl-$bOJ_ba+Y+-<PybA)[!4w|eH{mb~V&[OOQ]ogl');
define('NONCE_KEY',        'nfAZU6Y:{d$#XM`-}En$s=8X8dht{#Oo_7ieo!JRxKor>a+5#AmhlbDodC[F(LZQ');
define('AUTH_SALT',        'W)anR3e=SGC;J5Y|)A1k36*RatAW-!J@Bw#$1m44NWgjTADKMYJAdlHjb[DZ_K:8');
define('SECURE_AUTH_SALT', 'Mr`~@I4Wfsw+hEJY-:#)g{$UER.Sqa-f)GNX`lG@b9x&qYp;S%3S^1~k/-rqEHe)');
define('LOGGED_IN_SALT',   '8Ks^mZ@Omj*+]_S~b7gq}5+5embT/ZFj<$W]nXWx{O#^lReze}qe_N!y@-+!R/F?');
define('NONCE_SALT',       '&L7(N]R4AXY]M7r1WL7A?LJ2dE--O+Kw?bnp:]0~Q3|V!u:kitf,[Q79pzI]LFu=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
